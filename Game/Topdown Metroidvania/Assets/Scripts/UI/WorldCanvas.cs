﻿using UnityEngine;
using System.Collections;

public class WorldCanvas : MonoBehaviour
{
    public Transform damageNumberTransform;

    private void Awake()
    {
        Util.WorldCanvas = this;
    }

    public void CreateDamageNumber(float damage, Color color, Vector2 position)
    {
        var damageNumber = Instantiate(Util.PrefabHolder.damageNumberPrefab, damageNumberTransform);
        damageNumber.Init(damage.ToString(), color, position);
    }

    public void CreateDamageNumber(string text, Color color, Vector2 position)
    {
        var damageNumber = Instantiate(Util.PrefabHolder.damageNumberPrefab, damageNumberTransform);
        damageNumber.Init(text, color, position);
    }
}
