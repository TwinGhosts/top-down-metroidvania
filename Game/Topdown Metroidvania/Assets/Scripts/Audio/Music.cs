﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{
    [Header("Effects UI")]
    public AudioClip e_button_click;
    public AudioClip e_node_connect;
    public AudioClip e_node_disconnect;
    public AudioClip e_node_error;
    public AudioClip e_node_divide;
    public AudioClip e_win;

    [Header("Music")]
    public AudioClip m_game;

    [Header("Sources")]
    public AudioSource s_music;
    public AudioSource s_effects;

    public static Music Instance;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name != "Main Menu" && SceneManager.GetActiveScene().name != "Credits" || SceneManager.GetActiveScene().name != "Level Select")
        {
            if (s_music.clip != m_game)
            {
                StopMusic();
                PlayMusic(m_game, 0.15f, true);
            }
        }
    }

    public void PlaySound(int index)
    {
        s_effects.PlayOneShot(GetClipFromEnum((Audio)index), 0.2f);
    }

    public void PlaySound(Audio audio)
    {
        s_effects.PlayOneShot(GetClipFromEnum(audio), 0.3f);
    }

    public void PlaySound(Audio audio, float volume)
    {
        s_effects.PlayOneShot(GetClipFromEnum(audio), volume);
    }

    public void PlaySound(AudioClip clipToPlay, float volume)
    {
        s_effects.PlayOneShot(clipToPlay, volume);
    }

    public void PlayMusic(AudioClip musicClip, float volume, bool repeat)
    {
        s_music.clip = musicClip;
        s_music.volume = volume;
        s_music.Play();
        s_music.loop = repeat;
    }

    public void StopMusic()
    {
        s_music.Stop();
    }

    private AudioClip GetClipFromEnum(Audio audio)
    {
        switch (audio)
        {
            default:
            case Audio.Click:
                return e_button_click;
            case Audio.Connect:
                return e_node_connect;
            case Audio.Disconnect:
                return e_node_disconnect;
            case Audio.Error:
                return e_node_error;
            case Audio.Divide:
                return e_node_divide;
            case Audio.Win:
                return e_win;
        }
    }
}

public enum Audio
{
    Click,
    Connect,
    Disconnect,
    Error,
    Divide,
    Win
}