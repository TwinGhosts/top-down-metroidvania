﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapColliderUtil : MonoBehaviour
{
    public TileMapType type;

    private void Awake()
    {
        if (type == TileMapType.Pit) Util.PitCollider = this.GetComponent<TilemapCollider2D>();
        else if (type == TileMapType.Wall) Util.WallCollider = this.GetComponent<TilemapCollider2D>();
    }
}

public enum TileMapType
{
    Pit,
    Wall,
}