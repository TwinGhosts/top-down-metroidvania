﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public AnimationCurve itemFlyArc;

    public List<Entity> hostileEntities = new List<Entity>();
    public List<Entity> neutralEntities = new List<Entity>();
    public List<Entity> alliedEntities = new List<Entity>();

    // Use this for initialization
    private void Awake()
    {
        Util.GameManager = this;
    }
}
