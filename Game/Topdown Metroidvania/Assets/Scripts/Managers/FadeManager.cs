﻿using Com.LuisPedroFonseca.ProCamera2D;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    [SerializeField] private Graphic _fadeGraphic = null;
    private Color _color;
    private bool _isFading = false;

    [HideInInspector] public bool isFadedIn = false;
    [HideInInspector] public UnityEvent OnFadeOut;
    [HideInInspector] public UnityEvent OnFadeIn;

    private AsyncOperation loadOperation;

    private const float STANDARD_FADE_DURATION = 0.4f;

    private void Awake()
    {
        Util.FadeManager = this;

        _fadeGraphic.gameObject.SetActive(true);
    }

    // Use this for initialization
    private void Start()
    {
        _color = _fadeGraphic.color;
        Fade(true);
    }

    private void Update()
    {
        // Test for controlling the loading
        if (loadOperation != null && loadOperation.progress >= 0.9f)
        {
            loadOperation.allowSceneActivation = true;
        }
    }

    private void Fade(bool fadeIn)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, STANDARD_FADE_DURATION));
    }

    private void Fade(bool fadeIn, float duration = STANDARD_FADE_DURATION)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, duration));
    }

    #region Scene Switching
    // Reloads the current scene
    public void RestartScene(float duration = STANDARD_FADE_DURATION)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }
    }

    // Goes to a scene specified with a name
    public void GoToScene(string sceneName)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(sceneName));
        }
    }

    // Goes to a scene specified with a RoomScene reference
    public void GoToScene(RoomScene scene)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(StringUtil.AddSpacesToSentence(scene.ToString())));
        }
    }

    // Goes to a scene specified with an actual scene reference
    public void GoToScene(Scene scene)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, STANDARD_FADE_DURATION));
            OnFadeOut.AddListener(() => LoadAsync(scene.name));
        }
    }

    // Quits the application after fade out
    public void Quit(float duration = STANDARD_FADE_DURATION)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => Application.Quit());
        }
    }
    #endregion Scene Switching

    private void LoadAsync(string sceneName)
    {
        loadOperation = SceneManager.LoadSceneAsync(sceneName);
        loadOperation.allowSceneActivation = false;
    }

    // The actual graphic fading
    private IEnumerator _Fade(bool fadeIn, float duration)
    {
        // Variable settings
        _isFading = true;

        // Position player when respawning
        if (fadeIn && Util.respawnOnLoad)
        {
            Util.Player.Heal(100000f);
            Util.Player.spriteRenderer.enabled = true;
            Util.Player.transform.position = GameOptions.Instance.GetRespawnLocation();
            Util.Player.enableInput = true;
            Util.Player.isInvulnerable = false;

            Util.Player.buffs.Clear();
            Util.Player.debuffs.Clear();
        }

        // Local var setting
        var progress = 0f;
        var startColor = _fadeGraphic.color;
        var endColor = _color;
        endColor.a = (fadeIn) ? 0f : 1f;

        // Actual fading
        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            _fadeGraphic.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        // Variable resetting
        _isFading = false;
        isFadedIn = fadeIn;

        // Event calling
        if (fadeIn)
            OnFadeIn.Invoke();
        else
            OnFadeOut.Invoke();
    }
}
