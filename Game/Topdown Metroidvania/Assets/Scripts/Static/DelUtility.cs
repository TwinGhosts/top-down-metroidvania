﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DelUtility
{
    public static List<PickupDel> GetDelPieces (int value)
    {
        var tempList = new List<PickupDel>();

        while (value != 0)
        {
            if (value >= 100) { tempList.Add(Util.PrefabHolder.del100); value -= 100; }
            else if (value >= 25) { tempList.Add(Util.PrefabHolder.del25); value -= 25; }
            else if (value >= 5) { tempList.Add(Util.PrefabHolder.del5); value -= 5; }
            else if (value >= 1) { tempList.Add(Util.PrefabHolder.del1); value -= 1; }
        }

        return tempList;
    }
}
