﻿using UnityEngine;

public class AnimationUtility : MonoBehaviour
{
    public void ResetToIdle(string idleName)
    {
        GetComponentInParent<Animator>().Play(idleName);
    }
}
