﻿using UnityEngine;
using System.Collections;

public class WeaponRanged : Weapon
{
    public Projectile projectilePrefab;
    protected Projectile lastCreatedProjectile;

    public override void Attack()
    {
        base.Attack();
        lastCreatedProjectile = Instantiate(projectilePrefab, transform.position, Quaternion.Euler(0f, 0f, owner.aimAngle - 90f));
    }
}
