﻿using UnityEngine;
using UnityEngine.UI;

public class DamageNumber : MonoBehaviour
{
    [HideInInspector] public Text damageText;
    public float duration = 1f;
    public float distance = 3f;

    private RectTransform rect;
    private float progress = 0f;

    public AnimationCurve curve;

    private void Awake()
    {
        transform.localScale = Vector3.zero;
        damageText = GetComponent<Text>();
        rect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        if (Util.HUD && !transform.parent.Equals(Util.WorldCanvas.damageNumberTransform))
        {
            transform.SetParent(Util.WorldCanvas.damageNumberTransform);
        }
    }

    public void Init(string text, Color color, Vector2 position)
    {
        damageText.text = text;
        damageText.color = color;
        rect.anchoredPosition = position;
    }

    private void Update()
    {
        progress += Time.deltaTime;
        transform.localScale = Vector3.one * curve.Evaluate(progress);
        rect.anchoredPosition += Vector2.up * distance * Time.deltaTime;

        if (progress >= 1f || (progress > 0.5f && transform.localScale.x < 0.0005f)) Destroy(gameObject);
    }
}
