﻿using UnityEngine;
using System.Collections;

public class EffectDamageOverTime : EffectNegative
{
    private int ticks = 5;
    private int currentTick = 1;
    private float tickInterval;
    private bool tickOnApplication = true;
    private float damagePerTick;

    public EffectDamageOverTime(Entity target, float duration, float damagePerTick, int ticks, bool firstTick) : base(duration)
    {
        this.damagePerTick = damagePerTick;
        this.ticks = ticks;
        this.target = target;
        tickOnApplication = firstTick;

        // Calculate the amount of ticks
        tickInterval = duration / ticks;

        // Tick once when needed
        if (tickOnApplication)
        {
            target.Damage(damagePerTick, true);
        }
    }

    public override void Update()
    {
        base.Update();

        // Invoke the tick effect
        if (duration > tickInterval * currentTick)
        {
            currentTick++;
            target.Damage(damagePerTick, true);
        }
    }
}