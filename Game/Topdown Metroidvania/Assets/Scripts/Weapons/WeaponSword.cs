﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSword : Weapon
{
    [Header("Sword Properties")]
    public float swordSwingTime = 0.2f;
    public float swordSwingDistance = 100f;
    [HideInInspector] public float swordLiftTime = 0.15f;
    [HideInInspector] public float swordLiftDistance = 100f;
    public float swordResetTime = 0.1f;

    public Collider2D swingCollider;

    public override void Attack()
    {
        base.Attack();

        if (hasControl)
        {
            owner.OnWeaponHit.Invoke();
            StartCoroutine(_Attack());
        }
    }

    protected IEnumerator _Attack()
    {
        // Start
        swingCollider.enabled = true;
        isAttacking = true;
        hasControl = false;
        owner.hasAimControl = false;

        // Angular properties
        var progress = 0f;
        var angle = owner.aimAngle;

        var leftCheck = owner.aimAngle >= 90f || owner.aimAngle < -90f;
        var swordDistance = swordSwingDistance * (leftCheck ? -1f : 1f);

        var swingStartAngle = angle - swordDistance / 2f;
        var swingTargetAngle = angle + swordDistance / 2f;

        var resetStartAngle = swingTargetAngle;
        var resetTargetAngle = angle;

        /// Loop
        // Swing motion
        progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / swordSwingTime;
            owner.UpdateAim(Mathf.Lerp(swingStartAngle, swingTargetAngle, Mathf.SmoothStep(0f, 1f, progress)));
            yield return null;
        }

        // Reset motion
        progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / swordResetTime;
            owner.UpdateAim(Mathf.Lerp(resetStartAngle, resetTargetAngle, Mathf.SmoothStep(0f, 1f, progress)));
            yield return null;
        }

        // End
        swingCollider.enabled = false;
        hasControl = true;
        isAttacking = false;
        owner.hasAimControl = true;

        // Reset targets
        hitTargets.Clear();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Damage targets
        var entity = collision.GetComponent<Entity>();
        if (entity && !entity.isInvulnerable && !hitTargets.Contains(entity) && entity.team != owner.team)
        {
            hitTargets.Add(entity);
            entity.Damage(damage, owner);
            ProCamera2DShake.Instance.Shake(0.1f, Vector2.one);
            entity.Knockback(owner.transform.position, 10f);
            owner.OnSuccesfulWeaponHit.Invoke();
        }
        else if (entity && entity.isInvulnerable && entity.team != owner.team)
        {
            owner.OnFailedWeaponHit.Invoke();
        }
    }
}
