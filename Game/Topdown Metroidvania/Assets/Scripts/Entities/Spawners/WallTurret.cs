﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTurret : Activatable
{
    public Transform bulletSpawnLocation;
    public Projectile projectile;
    public float shootInterval = 1.5f;
    public Direction shootDirection = Direction.Down;

    public float projectileDamage = 10f;
    public float projectileSpeed = 10f;

    public string idleAnimationName;
    public string shootAnimationName;

    private Vector2 shootVector;
    private float shootTimer = 0f;

    private void Awake()
    {
        // Set the initial direction
        ChangeDirection(shootDirection);
    }

    private void Update()
    {
        if (!isActive) return;

        // Increment the shoot timer in seconds
        shootTimer += Time.deltaTime;

        // Shoot
        if (shootTimer >= shootInterval)
        {
            shootTimer = 0f;

            GetComponent<Animator>().Play(idleAnimationName);
            GetComponent<Animator>().Play(shootAnimationName);

            var bullet = Instantiate(projectile, bulletSpawnLocation.position, Quaternion.identity);
            bullet.Init(projectileSpeed, projectileDamage, MathEx.AngleFromVector(shootVector), Util.NullObject, null, 10f);
        }
    }

    public override void Activate(bool activate)
    {
        isActive = !isActive;
    }

    public void ChangeDirection(Direction newDirection)
    {
        shootDirection = newDirection;
        shootVector = VectorFromDirection(newDirection);
    }

    private Vector2 VectorFromDirection(Direction dir)
    {
        if (dir == Direction.Down) return Vector2.down;
        if (dir == Direction.Up) return Vector2.up;
        if (dir == Direction.Left) return Vector2.left;
        if (dir == Direction.Right) return Vector2.right;

        return Vector2.down;
    }

    public void Reset()
    {
        GetComponent<Animator>().Play(idleAnimationName);
    }
}
