﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RestInteractable : Interactable
{
    [Header("Rest Variables")]
    public float restDuration = 1.5f;
    public float timeIntervals = 0.05f;

    [Header("Log Variable")]
    public string id = "";

    [Header("Reactive Particle Systems")]
    public ParticleSystemRenderer centerSystemRenderer;
    public List<ParticleSystem> systems = new List<ParticleSystem>();

    protected override void Awake()
    {
        base.Awake();

        centerSystemRenderer.sortingOrder = (int)(-centerSystemRenderer.gameObject.transform.position.y * 32) + 1;

        // Enable the particle systems
        foreach (var system in systems)
        {
            system.gameObject.GetComponent<ParticleSystemRenderer>().sortingOrder = (int)(-system.gameObject.transform.position.y * 32);
        }
    }

    public override void Interact()
    {
        // Start the rest animation
        StartCoroutine(_Rest());
    }

    private IEnumerator _Rest()
    {
        // Local variables
        var progress = 0f;
        var playerStartingHealth = Util.Player.health;
        var playerHealthDifference = Util.Player.maxHealth - playerStartingHealth;

        GameOptions.Instance.Rest(id);

        // Enable the particle systems
        foreach (var system in systems)
        {
            system.Play();
        }

        // Start settings
        isActive = false;
        Util.Player.enableInput = false;
        Util.Player.isInvulnerable = true;

        // Play through the animation
        while (progress < restDuration)
        {
            // Count up the time
            progress += timeIntervals;

            // TODO: Make player flicker or raise etc...

            // Refill player health over time
            Util.Player.Heal(playerHealthDifference * timeIntervals);

            // Wait for a fraction
            yield return new WaitForSeconds(timeIntervals);
        }

        // Enable the particle systems
        foreach (var system in systems)
        {
            system.Stop();
        }

        // Reset start settings
        isActive = true;
        Util.Player.enableInput = true;
        Util.Player.isInvulnerable = false;
    }
}
