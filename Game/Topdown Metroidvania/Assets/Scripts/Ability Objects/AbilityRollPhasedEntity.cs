﻿using UnityEngine;
using System.Collections;

public class AbilityRollPhasedEntity : MonoBehaviour
{
    public float fadeDuration = 0.7f;
    [HideInInspector] public ParticleSystem ps;

    public Sprite sprite;

    private float progress = 0f;
    private Color startColor;
    private Color endColor;

    private SpriteRenderer spriteRenderer;
    private Shader fullWhiteShader;

    // Use this for initialization
    private void Awake()
    {
        ps = GetComponent<ParticleSystem>();

        startColor = endColor = GetComponent<SpriteRenderer>().color;
        endColor.a = 0f;

        spriteRenderer = GetComponent<SpriteRenderer>();
        fullWhiteShader = Shader.Find("GUI/Text Shader");
        spriteRenderer.material.shader = fullWhiteShader;
    }

    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        // Fade color out
        if (progress < 1f)
        {
            progress += Time.deltaTime / fadeDuration;
            spriteRenderer.color = Color.Lerp(startColor, endColor, progress);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
