﻿using UnityEngine;
using System.Collections;

public class AbilityStalkerTeleport : Ability
{
    [Header("Teleport Specifics")]
    public float teleportDuration = 1f;
    public Vector2 teleportBackRange = new Vector2(-50, 50);
    [HideInInspector] public Vector2 teleportMidPoint;

    [Header("Sub-systems")]
    public ParticleSystem teleportParticleSystem;
    public LineRenderer teleportPathRenderer;

    private float progress = 0f;
    private bool effectActive = false;
    private bool midPointReached = false;

    protected override void Update()
    {
        base.Update();

        // The basic effect
        if (effectActive)
        {
            progress += Time.deltaTime / (teleportDuration / 2f);

            // Midpoint part
            if (progress <= 1f && !midPointReached)
            {

            }
            else if (progress > 1f && !midPointReached)
            {

            }

            // End part
            if (progress <= 1f && midPointReached)
            {

            }
            else if (progress > 1f && midPointReached)
            {

            }
        }
    }

    protected override void CastAction()
    {
        if (owner && !effectActive)
        {
            progress = 0f;
            effectActive = true;
            midPointReached = false;

            // Set start
        }
    }

    private void Reset()
    {

    }
}
