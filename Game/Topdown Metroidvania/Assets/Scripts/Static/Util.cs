﻿using UnityEngine;
using UnityEngine.Tilemaps;

public static class Util
{
    public static FadeManager FadeManager;
    public static EntityPlayer Player;
    public static GameManager GameManager;
    public static GameObject DelGatherPoint;
    public static HUD HUD;
    public static WorldCanvas WorldCanvas;
    public static PrefabHolder PrefabHolder;
    public static DialogueManager DialogueManager;
    public static ColorHolder ColorHolder;
    public static Room Room;
    public static EntityHostile NullObject;

    public static TilemapCollider2D PitCollider;
    public static TilemapCollider2D WallCollider;

    public static bool respawnOnLoad = false;
    public static float playerHealthIncrement = 20f;
}
