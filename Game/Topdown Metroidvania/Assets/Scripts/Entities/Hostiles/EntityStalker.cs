﻿using UnityEngine;
using System.Collections;

public class EntityStalker : EntityHostile
{
    protected override void Start()
    {
        base.Start();

        state = StandardStates.Patrolling;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void StateCheck()
    {
        base.StateCheck();
    }

    protected override void AttackingBehaviour()
    {
        base.AttackingBehaviour();
    }
}
