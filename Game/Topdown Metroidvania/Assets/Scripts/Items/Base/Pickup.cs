﻿using UnityEngine;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using System;

public class Pickup : MonoBehaviour
{
    public bool animatePickUpAsCurrency = false;

    [HideInInspector] public bool hasBeenPickedUp = false;

    [HideInInspector] public Vector2 startPoint, endPoint;
    protected float progress = 0f;
    protected float flyDuration = 0.45f;

    public bool sortDepth = true;

    // Update is called once per frame
    protected virtual void Update()
    {
        NormalJob();
    }

    public void OnPickup()
    {
        Util.Player.inventory.Pickup(this);
        Destroy(gameObject);
    }

    protected void NormalJob()
    {
        if (sortDepth) GetComponent<SpriteRenderer>().sortingOrder = (int)(-transform.position.y * 32);

        // Move towards player
        if (hasBeenPickedUp)
        {
            // Animate as curreny or not
            if (animatePickUpAsCurrency)
            {
                GetComponent<Collider2D>().enabled = false;
                endPoint = Camera.main.ScreenToWorldPoint(Util.DelGatherPoint.transform.position);
                transform.position = Vector2.Lerp(startPoint, endPoint, Util.GameManager.itemFlyArc.Evaluate(progress));
                progress = Mathf.Clamp(progress + Time.deltaTime / flyDuration, 0f, 1f);

                // When finished animating, collect
                if (progress >= 1f)
                {
                    // Handle the picking up behaviour
                    OnPickup();
                }
            }
            else
            {
                OnPickup();
            }
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<EntityPlayer>();
        if (player && !hasBeenPickedUp)
        {
            hasBeenPickedUp = true;
            startPoint = transform.position;
        }
    }
}
