﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    [Header("Basic Ability Properties")]
    public new string name = "Ability Name";
    [TextArea]
    public string description = "Ability Description";
    public float castTime = 2f;
    public float cooldown = 5f;
    public float initialCooldown = 0f;
    protected float maxCooldown;
    protected Entity owner;

    [HideInInspector] public bool isCasting = false;
    [HideInInspector] public float currentCastTime = 0f;
    [HideInInspector] public bool isActive = true;
    [HideInInspector] public Action castFinishedAction = () => { };

    public bool IsOnCooldown { get { return cooldown > 0f; } }

    protected void Awake ()
    {
        maxCooldown = cooldown;
        cooldown = initialCooldown;
        owner = GetComponentInParent<Entity>();
    }

    protected virtual void Update ()
    {
        // Cooldown
        if (owner.team == Team.Hostile && owner.state == Entity.StandardStates.Attacking)
        {
            cooldown -= Time.deltaTime;
        }
        else if (owner.team == Team.Allied)
        {
            cooldown -= Time.deltaTime;
        }

        // Casting
        if (isCasting)
        {
            // While Casting
            if (currentCastTime < castTime)
            {
                currentCastTime += Time.deltaTime;
            }
            // When finished casting
            else
            {
                OnCastEnd();
            }
        }
    }

    public virtual void Cast ()
    {
        if (cooldown <= 0f)
        {
            cooldown = maxCooldown;
            OnCastStart();
        }
    }

    protected virtual void OnCastStart ()
    {
        isCasting = true;
    }

    protected virtual void OnCastEnd ()
    {
        isCasting = false;
        currentCastTime = 0f;
        CastAction();
        castFinishedAction.Invoke();
    }

    protected virtual void CastAction () { }
}
