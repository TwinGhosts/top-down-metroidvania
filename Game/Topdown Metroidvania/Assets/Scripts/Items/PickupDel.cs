﻿using UnityEngine;
using System.Collections;

public class PickupDel : Pickup
{
    public int del = 5;
    public float autoPickUpDelay = 2f;

    protected override void Update()
    {
        base.Update();

        autoPickUpDelay -= Time.deltaTime;
        if (autoPickUpDelay <= 0f && !hasBeenPickedUp)
        {
            startPoint = transform.position;
            hasBeenPickedUp = true;
        }
    }
}
