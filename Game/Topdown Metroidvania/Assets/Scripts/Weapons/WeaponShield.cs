﻿using UnityEngine;

public class WeaponShield : Weapon
{
    [Header("Shield Properties")]
    public float parryTimeSpan = 0.2f;
    protected float currentParryTimeSpan = 0f;
}
