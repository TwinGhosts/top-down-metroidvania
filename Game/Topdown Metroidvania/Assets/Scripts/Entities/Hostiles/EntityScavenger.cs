﻿using UnityEngine;
using System.Collections;

public class EntityScavenger : EntityHostile
{
    protected override void Start()
    {
        base.Start();

        state = StandardStates.Patrolling;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void StateCheck()
    {
        base.StateCheck();
    }
}
