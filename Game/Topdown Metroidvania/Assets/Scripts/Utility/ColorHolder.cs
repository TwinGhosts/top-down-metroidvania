﻿using UnityEngine;
using System.Collections;

public class ColorHolder : MonoBehaviour
{
    private void Awake()
    {
        Util.ColorHolder = this;
        SetStatic();
    }

    private void SetStatic()
    {
        C.lightred = Util.ColorHolder.lightred;
        C.lightorange = Util.ColorHolder.lightorange;
        C.lightyellow = Util.ColorHolder.lightyellow;
        C.lightswamp = Util.ColorHolder.lightswamp;
        C.lightgreen = Util.ColorHolder.lightgreen;
        C.lightmint = Util.ColorHolder.lightmint;
        C.lightcyan = Util.ColorHolder.lightcyan;
        C.lightblue = Util.ColorHolder.lightblue;
        C.lightpurple = Util.ColorHolder.lightpurple;
        C.lightpink = Util.ColorHolder.lightpink;

        C.red = Util.ColorHolder.red;
        C.orange = Util.ColorHolder.orange;
        C.yellow = Util.ColorHolder.yellow;
        C.swamp = Util.ColorHolder.swamp;
        C.green = Util.ColorHolder.green;
        C.mint = Util.ColorHolder.mint;
        C.cyan = Util.ColorHolder.cyan;
        C.blue = Util.ColorHolder.blue;
        C.purple = Util.ColorHolder.purple;
        C.pink = Util.ColorHolder.pink;

        C.darkred = Util.ColorHolder.darkred;
        C.darkorange = Util.ColorHolder.darkorange;
        C.darkyellow = Util.ColorHolder.darkyellow;
        C.darkswamp = Util.ColorHolder.darkswamp;
        C.darkgreen = Util.ColorHolder.darkgreen;
        C.darkmint = Util.ColorHolder.darkmint;
        C.darkcyan = Util.ColorHolder.darkcyan;
        C.darkblue = Util.ColorHolder.darkblue;
        C.darkpurple = Util.ColorHolder.darkpurple;
        C.darkpink = Util.ColorHolder.darkpink;

        C.black = Color.black;
        C.darkgray = Util.ColorHolder.darkgray;
        C.gray = Util.ColorHolder.gray;
        C.lightgray = Util.ColorHolder.lightgray;
        C.white = Color.white;
    }

    [Header("Light")]
    public Color lightred = Color.black;
    public Color lightorange = Color.black;
    public Color lightyellow = Color.black;
    public Color lightswamp = Color.black;
    public Color lightgreen = Color.black;
    public Color lightmint = Color.black;
    public Color lightcyan = Color.black;
    public Color lightblue = Color.black;
    public Color lightpurple = Color.black;
    public Color lightpink = Color.black;

    [Header("Normal")]
    public Color red = Color.black;
    public Color orange = Color.black;
    public Color yellow = Color.black;
    public Color swamp = Color.black;
    public Color green = Color.black;
    public Color mint = Color.black;
    public Color cyan = Color.black;
    public Color blue = Color.black;
    public Color purple = Color.black;
    public Color pink = Color.black;

    [Header("Dark")]
    public Color darkred = Color.black;
    public Color darkorange = Color.black;
    public Color darkyellow = Color.black;
    public Color darkswamp = Color.black;
    public Color darkgreen = Color.black;
    public Color darkmint = Color.black;
    public Color darkcyan = Color.black;
    public Color darkblue = Color.black;
    public Color darkpurple = Color.black;
    public Color darkpink = Color.black;

    [Header("Gray Tones")]
    public Color black = Color.black;
    public Color darkgray = Color.black;
    public Color gray = Color.black;
    public Color lightgray = Color.black;
    public Color white = Color.white;
}

public static class C
{
    #region STATIC
    public static Color lightred = Color.black;
    public static Color lightorange = Color.black;
    public static Color lightyellow = Color.black;
    public static Color lightswamp = Color.black;
    public static Color lightgreen = Color.black;
    public static Color lightmint = Color.black;
    public static Color lightcyan = Color.black;
    public static Color lightblue = Color.black;
    public static Color lightpurple = Color.black;
    public static Color lightpink = Color.black;

    public static Color red = Color.black;
    public static Color orange = Color.black;
    public static Color yellow = Color.black;
    public static Color swamp = Color.black;
    public static Color green = Color.black;
    public static Color mint = Color.black;
    public static Color cyan = Color.black;
    public static Color blue = Color.black;
    public static Color purple = Color.black;
    public static Color pink = Color.black;

    public static Color darkred = Color.black;
    public static Color darkorange = Color.black;
    public static Color darkyellow = Color.black;
    public static Color darkswamp = Color.black;
    public static Color darkgreen = Color.black;
    public static Color darkmint = Color.black;
    public static Color darkcyan = Color.black;
    public static Color darkblue = Color.black;
    public static Color darkpurple = Color.black;
    public static Color darkpink = Color.black;

    public static Color black = Color.black;
    public static Color darkgray = Color.black;
    public static Color gray = Color.black;
    public static Color lightgray = Color.black;
    public static Color white = Color.white;
    #endregion STATIC
}