﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBow : WeaponRanged
{
    [Header("Bow Properties")]
    public float maxChargeTime = 2f;
    public float chargeMaxDamage = 40f;
    public float chargeMinDamage = 5f;

    [Header("Projectile Properties")]
    public float projectileSpeed = 16f;

    protected float currentChargeTime = 0f;
    protected float currentTimeFraction = 0f;

    private const string anim_idle = "Bow Idle";
    private const string anim_charge = "Bow Pull Back";
    private const string anim_release = "Bow Release";

    protected override void Awake()
    {
        base.Awake();
        animator = GetComponentInChildren<Animator>();
    }

    protected override void Update()
    {
        base.Update();
    }

    public void Charge()
    {
        animator.Play(anim_charge);

        currentChargeTime = Mathf.Clamp(currentChargeTime + Time.deltaTime, 0f, maxChargeTime);
        currentTimeFraction += Time.deltaTime / maxChargeTime;
        damage = Mathf.Lerp(chargeMinDamage, chargeMaxDamage, currentTimeFraction);
    }

    public override void Attack()
    {
        base.Attack();
        if (hasControl)
        {
            animator.Play(anim_release);
            lastCreatedProjectile.Init(projectileSpeed, damage, owner.aimAngle, owner, this, 5f);
            owner.OnWeaponHit.Invoke();
        }

        currentChargeTime = 0f;
        currentTimeFraction = 0f;
    }
}
