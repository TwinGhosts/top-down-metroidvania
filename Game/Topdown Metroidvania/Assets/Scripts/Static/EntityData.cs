﻿public static class EntityData
{
    public static EntityDataWrapper GetData(EntityType hostile)
    {
        switch (hostile)
        {
            default:
            case EntityType.Player:
                return PlayerData;
            case EntityType.Scavenger:
                return ScavengerData;
            case EntityType.Stalker:
                return StalkerData;
            case EntityType.Trapper:
                return TrapperData;
        }
    }

    public static EntityDataWrapper PlayerData = new EntityDataWrapper("Player", "Description");
    public static EntityDataWrapper ScavengerData = new EntityDataWrapper("Scavenger", "Description");
    public static EntityDataWrapper StalkerData = new EntityDataWrapper("Stalker", "Description");
    public static EntityDataWrapper TrapperData = new EntityDataWrapper("Trapper", "Description");
}

public struct EntityDataWrapper
{
    public string name;
    public string description;

    public EntityDataWrapper(string name, string description)
    {
        this.name = name;
        this.description = description;
    }
}
