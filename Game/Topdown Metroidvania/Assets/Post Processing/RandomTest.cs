﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class RandomTest : MonoBehaviour
{
    private PostProcessVolume volume;
    private ChromaticAberration aberration;

    private void Awake ()
    {
        volume = GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out aberration);
    }

    private void Update ()
    {
        aberration.intensity.value = Mathf.PingPong(Time.time * 10f, 5);
    }
}
