﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerTargeter : MonoBehaviour
{
    public float radius = 1f;
    public float movementSpeed = 2000f;
    private Rewired.Player inputController;

    private void Awake ()
    {
        inputController = Rewired.ReInput.players.GetPlayer(0);
    }

    private void Start ()
    {
        // Add this as a camera target at the start
        ProCamera2D.Instance.AddCameraTarget(transform);
    }

    // Update is called once per frame
    /*private void Update ()
   {
       // Movement variables
       var hForce = inputController.GetAxisRaw("Move Camera Horizontal");
       var vForce = inputController.GetAxisRaw("Move Camera Vertical");
       var finalForce = new Vector2(hForce, vForce) * movementSpeed * Time.deltaTime;

       // Positional variables
       var centerPosition = Util.Player.transform.position;
       var distance = Vector3.Distance(transform.position, centerPosition);

       // Add the final force to the position
       transform.position += (Vector3)finalForce;

       // If the distance is less than the radius, it is already within the circle
       if (distance > radius)
       {
           var fromOriginToObject = transform.position - centerPosition;
           fromOriginToObject *= radius / distance;

           var finalPosition = centerPosition + fromOriginToObject;
           transform.position = finalPosition;
       }

       // When there is no input, reset slowly
       if (finalForce == Vector2.zero)
       {
           timer -= Time.deltaTime;
           timer = Mathf.Clamp(timer, 0f, 1f);
           transform.position = Vector3.Lerp(transform.position, centerPosition, Mathf.SmoothStep(1f, 0f, timer));
       }
       else
       {
           timer = 1f;
       }
   }*/
}
