﻿using UnityEngine;
using System.Collections;

public class PrefabHolder : MonoBehaviour
{
    [Header("Weapons")]
    public WeaponBow bow;
    public WeaponSword sword;
    public WeaponSpear spear;
    public WeaponShield shield;

    [Header("Del")]
    public PickupDel del1;
    public PickupDel del5;
    public PickupDel del25;
    public PickupDel del100;

    [Header("UI")]
    public Sprite[] healthFractions;
    public UIHealthObject healthObject;
    public DamageNumber damageNumberPrefab;

    private void Awake()
    {
        Util.PrefabHolder = this;
    }
}
