﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EntranceExit : MonoBehaviour
{
    [Header("The Room you came from")]
    public RoomScene entranceRoom;
    public EnterDirection enterDirection;

    [Header("The Room you go to")]
    public RoomScene exitRoom;
    public string exitKey;

    private void Start()
    {
        Util.Room.entrancesExits.Add(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // When the player enter the exit box, go to other room
        if (Util.Player.state != Entity.StandardStates.Entering && collision.GetComponent<EntityPlayer>())
        {
            // Fade to the next scene
            Util.FadeManager.GoToScene(exitRoom);

            // Switch Rooms
            GameOptions.Instance.SwitchRoom(exitRoom, enterDirection, exitKey);
        }
    }
}
