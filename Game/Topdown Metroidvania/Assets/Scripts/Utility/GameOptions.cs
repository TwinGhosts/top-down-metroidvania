﻿using UnityEngine;
using System.Collections;

public class GameOptions : MonoBehaviour
{
    public static GameOptions Instance;

    public RoomScene currentRoom;
    public RoomScene previousRoom;
    public EnterDirection enterDirection;
    public string exitKey;
    public bool startEnter = false;

    public RoomScene lastRestedScene;
    public string restLocationID = "";

    private void Awake()
    {
        // Persistence
        if (Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        currentRoom = FindObjectOfType<Room>().scene;
    }

    public void Rest(string restID)
    {
        lastRestedScene = currentRoom;
        restLocationID = restID;
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        startEnter = true;
    }

    public Vector2 GetRespawnLocation()
    {
        // Find the correct respawn location based on the interactables in the room
        foreach (var restObject in FindObjectsOfType<RestInteractable>())
        {
            // Compare the ids
            if (restObject.id.Equals(restLocationID))
            {
                return restObject.transform.position;
            }
        }

        // Error when none have been found
        Debug.LogWarning("WARNING > No Restobject found to teleport to!");
        return Vector2.zero;
    }

    public void SwitchRoom(RoomScene exitRoom, EnterDirection direction, string exitKey)
    {
        Instance.previousRoom = Instance.currentRoom;
        Instance.currentRoom = exitRoom;
        enterDirection = direction;
        this.exitKey = exitKey;
    }
}
