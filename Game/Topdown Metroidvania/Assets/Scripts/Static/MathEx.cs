﻿using UnityEngine;

public static class MathEx
{
    public static Vector3 VectorFromAngle (float dir)
    {
        return new Vector3(Mathf.Cos(Mathf.Deg2Rad * dir), Mathf.Sin(Mathf.Deg2Rad * dir));
    }

    public static float AngleFromVector (Vector2 dir)
    {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public static float Clamp0360 (float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result;
    }

    public static float ClampMinus180Positive180 (float angle)
    {
        var result = angle - Mathf.CeilToInt(angle / 360f) * 360f;

        if (result < 0)
        {
            result += 360f;
        }

        return result - 180f;
    }

}