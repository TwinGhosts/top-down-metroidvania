﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthObject : MonoBehaviour
{
    public int id = 0;
    public Image image;

    private void Awake ()
    {
        image = GetComponent<Image>();
    }
}
