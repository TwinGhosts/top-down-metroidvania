﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class ProjectileFireBall : Projectile
{
    private float activationTime = 0.3f;

    private void LateUpdate()
    {
        if (activationTime > 0f)
            activationTime -= Time.deltaTime;
        else if (!GetComponent<Collider2D>().enabled)
            GetComponent<Collider2D>().enabled = true;


        // Flip renderer
        if (Time.frameCount % 5 == 0)
        {
            GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        }
    }
}
