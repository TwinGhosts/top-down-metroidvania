﻿using UnityEngine;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;

public class Room : MonoBehaviour
{
    [Header("Basic Room Data")]
    public RoomScene scene;
    public new string name = "Room Name";
    public Area area;

    [Header("Linked Rooms")]
    public List<RoomScene> linkedRooms = new List<RoomScene>();

    [HideInInspector] public List<EntranceExit> entrancesExits = new List<EntranceExit>();

    private void Awake()
    {
        Util.Room = this;
    }

    private IEnumerator Start()
    {
        // Wait for one frame
        yield return null;

        // Enter the room
        if (GameOptions.Instance.startEnter && !Util.respawnOnLoad)
        {
            // Try to match the current exit key with an existing entrance
            foreach (var entranceExit in entrancesExits)
            {
                // If it matches, move the player to it
                if (GameOptions.Instance.exitKey.Equals(entranceExit.exitKey))
                {
                    // Set the player to the entering state
                    Util.Player.state = Entity.StandardStates.Entering;
                    Util.Player.transform.position = entranceExit.transform.position;

                    // Determine the entrace direction distance
                    var factor = 1f;
                    if (GameOptions.Instance.enterDirection == EnterDirection.Down) Util.Player.enterTarget = Vector2.up * factor;
                    else if (GameOptions.Instance.enterDirection == EnterDirection.Left) Util.Player.enterTarget = Vector2.right * factor;
                    else if (GameOptions.Instance.enterDirection == EnterDirection.Up) Util.Player.enterTarget = Vector2.down * factor;
                    else if (GameOptions.Instance.enterDirection == EnterDirection.Right) Util.Player.enterTarget = Vector2.left * factor;

                    // Set the entrance enter location
                    Util.Player.enterTarget += (Vector2)entranceExit.transform.position;

                    // Stop looping as we found the position
                    break;
                }
            }
        }

        // Clear all camera targets for positioning
        ProCamera2D.Instance.RemoveAllCameraTargets();

        // Position Camera
        var camPos = Util.Player.transform.position;
        camPos.z = Camera.main.transform.position.z;
        ProCamera2D.Instance.MoveCameraInstantlyToPosition(camPos);

        // Wait until the camera has faded in to re-assign the player as a target
        yield return new WaitUntil(() => Util.FadeManager.isFadedIn);

        // Add this as a camera target at the start
        ProCamera2D.Instance.AddCameraTarget(Util.Player.transform);

        // Add the player back to the allied unit's group
        Util.GameManager.alliedEntities.Add(EntityPlayer.Instance);
        Util.respawnOnLoad = false;
    }

    private void Update()
    {
        // TODO
    }
}