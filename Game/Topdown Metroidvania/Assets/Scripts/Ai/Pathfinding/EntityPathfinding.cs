﻿using Pathfinding;
using UnityEngine;
using UnityEngine.Events;

public class EntityPathfinding : MonoBehaviour
{
    public Path path;
    public float visualMovementSpeed = 15f;

    [HideInInspector]
    public bool isMoving = false;

    [HideInInspector]
    public Seeker seeker;
    [HideInInspector]
    public AIPath aiPath;

    [HideInInspector]
    public UnityEvent OnPathCompletion = new UnityEvent();

    public void Awake()
    {
        seeker = GetComponent<Seeker>();
        aiPath = GetComponent<AIPath>();
        OnPathCompletion.AddListener(() => isMoving = false);
    }

    public void Update()
    {
        aiPath.destination = Util.Player.transform.position;
    }
}