﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance;

    public int del = 0;
    public List<Item> items = new List<Item>();

    private void Awake()
    {
        // Persistence
        if (Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public bool HasItem(Item item)
    {
        return items.Contains(item);
    }

    public bool AddItem(Item item)
    {
        return true;
    }

    public void Pickup(Pickup pickup)
    {
        if (pickup is PickupDel)
        {
            del += (pickup as PickupDel).del;
            Util.HUD.AddDel();
        }
        else if (pickup is PickUpHealingPotion)
        {
            var potion = (pickup as PickUpHealingPotion);
            Util.Player.AddEffect(new EffectHealOverTime(Util.Player, potion.duration, potion.healthAmountPerTick, potion.ticks, potion.firstTickActive));
        }
    }
}
