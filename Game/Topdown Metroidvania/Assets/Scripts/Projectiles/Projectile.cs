﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

public class Projectile : MonoBehaviour
{
    public float speed;
    public float damage;
    public float angle;
    public Entity owner;
    public Team team;
    public Weapon weapon;
    public int maxTargetsHit = 1;
    public float duration = DUR_STATIC;
    protected const float DUR_STATIC = -10000f;
    public bool updateAngle = false;

    [HideInInspector] public List<Entity> targetsHit = new List<Entity>();

    protected UnityEvent OnHitEntity = new UnityEvent();

    public void Init(float speed, float damage, float angle, Entity owner, Weapon weapon, float duration = 7f)
    {
        this.speed = speed;
        this.damage = damage;
        this.angle = angle;
        this.owner = owner;
        if (owner) team = owner.team;
        this.duration = duration;
    }

    protected void Update()
    {
        // Move into the direction the projectile is facing
        transform.position += MathEx.VectorFromAngle(angle).normalized * speed * Time.deltaTime;

        // Keeps updating the angle with the rotation
        if (updateAngle)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, angle - 90f);
        }

        // Destroy the arrow when its duration has expired
        if (duration != DUR_STATIC)
        {
            duration -= Time.deltaTime;
            if (duration <= 0f && duration > DUR_STATIC)
            {
                Destroy(gameObject);
            }
        }
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        // Collision with walls etc.
        var other = collision.gameObject.GetComponent<MainCollision>();
        Debug.Log(other);

        if (other)
        {
            owner?.OnFailedWeaponHit.Invoke();
            Destroy(gameObject);
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        // Collision with walls etc.
        var other = collision.GetComponent<MainCollision>();

        if (other)
        {
            owner?.OnFailedWeaponHit.Invoke();
            Destroy(gameObject);
        }
    }
}
