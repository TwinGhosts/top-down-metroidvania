﻿using Pathfinding;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Entity : MonoBehaviour
{
    #region Variables
    [HideInInspector] public new string name = "Entity";
    [HideInInspector] public string description = "A basic entity";
    public EntityType entityType;
    [HideInInspector] public EntityDataWrapper entityData;

    [Min(0)] public float health = 100f;
    [Min(0)] public float maxHealth = 100f;

    [Min(0)] public float mana = 50f;
    [Min(0)] public float maxMana = 50f;

    [HideInInspector] public float movementSpeed;
    [Min(0)] public float maxMovementSpeed = 1000f;

    [HideInInspector] public float aimAngle = 0f;
    [HideInInspector] public Vector2 aimVector = Vector2.zero;

    [HideInInspector] public Rigidbody2D body;
    [HideInInspector] public SpriteRenderer spriteRenderer;
    [HideInInspector] public bool hasAimControl = true;
    public Transform depthSortTransform;

    [Header("Weapon")]
    public Weapon currentWeapon;
    public Transform weaponAnchor;
    public float attackRange = 5f;
    public float attackCooldown = 1f;
    protected float ATTACK_COOLDOWN;
    protected float attackMovementDelay = 0.5f;
    protected float ATTACK_MOVEMENT_DELAY;

    [HideInInspector] public UnityEvent OnSuccesfulWeaponHit = new UnityEvent();
    [HideInInspector] public UnityEvent OnWeaponHit = new UnityEvent();
    [HideInInspector] public UnityEvent OnFailedWeaponHit = new UnityEvent();

    [Header("Special Checks")]
    public bool isInvulnerable = false;
    public bool canPatrol = true;
    [Range(0, 100)] public float patrolRadius = 1f;
    [Range(0, 100)] public float patrolMinRandomWaitTime = 1f;
    [Range(0, 100)] public float patrolMaxRandomWaitTime = 5f;
    protected float patrolCurrentWaitTime = 0f;
    public bool canChase = true;
    [Range(0, 100)] public float aggroRadius = 4f;
    public bool canBeHacked = true;
    public bool canBeFeared = true;
    public bool canBeStunned = true;

    [Header("Team")]
    public Team team = Team.Neutral;
    protected Team TEAM = Team.Neutral;
    public int priority = 1;

    [Header("Effects")]
    public List<EffectPositive> buffs = new List<EffectPositive>();
    public List<EffectNegative> debuffs = new List<EffectNegative>();

    protected Color startColor;
    protected Color damageColor = Color.red;

    protected Entity lastDamageDealingEntity = null;
    protected Team lastDamageDealingTeam;
    [HideInInspector] public Vector2 input;
    public bool enableInput = true;
    [HideInInspector] public Vector2 currentInput;
    [HideInInspector] public Vector2 startPosition;

    // Durations and behaviour vars
    [HideInInspector] public float hackedDuration = 0f;
    [HideInInspector] public float stunDuration = 0f;
    [HideInInspector] public float fearDuration = 0f;
    [HideInInspector] public Vector2 nextPatrolPoint;
    [HideInInspector] public Entity target;

    [Header("Abilities")]
    public List<Ability> abilities = new List<Ability>();
    [HideInInspector] public Ability currentlyCastedAbility;

    protected Animator animator;

    protected AIPath pathfinder;

    [Header("State")]
    public StandardStates state = StandardStates.Idle;

    public enum StandardStates
    {
        None,
        Idle,
        Patrolling,
        Attacking,
        Casting,
        Chasing,
        Dying,
        Stunned,
        Feared,
        Hacked,
        Entering,
    }
    #endregion Variables

    protected virtual void Awake()
    {
        // Set the entity data
        entityData = EntityData.GetData(entityType);
        name = entityData.name;
        description = entityData.description;
        startPosition = transform.position;
        animator = GetComponentInChildren<Animator>();

        nextPatrolPoint = transform.position;
        TEAM = team;
        movementSpeed = maxMovementSpeed;
        body = GetComponentInChildren<Rigidbody2D>();

        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        startColor = spriteRenderer.color;

        if (currentWeapon) currentWeapon.owner = this;
        ATTACK_COOLDOWN = attackCooldown;
        ATTACK_MOVEMENT_DELAY = attackMovementDelay;

        pathfinder = GetComponentInChildren<AIPath>();
    }

    protected virtual void Start()
    {
        AddToTeam();

        // Store the entities as a parent of the entities object
        var parent = GameObject.Find("Entities");
        if (parent)
            transform.SetParent(parent.transform);
    }

    protected virtual void Update()
    {
        if (spriteRenderer.color != startColor) spriteRenderer.color = Color.Lerp(spriteRenderer.color, startColor, Time.deltaTime / 0.5f);
        if (depthSortTransform) spriteRenderer.sortingOrder = (int)(-depthSortTransform.position.y * 32);

        UpdateEffects();
        UpdateMovement();
        StateCheck();
    }

    protected virtual void StateCheck()
    {
        switch (state)
        {
            case StandardStates.None:
                break;
            default:
            case StandardStates.Idle:
                IdleBehaviour();
                SearchForTarget();
                break;
            case StandardStates.Patrolling:
                PatrollingBehaviour();
                SearchForTarget();
                break;
            case StandardStates.Attacking:
                AttackingBehaviour();
                break;
            case StandardStates.Chasing:
                ChasingBehaviour();
                break;
            case StandardStates.Casting:
                CastingBehaviour();
                break;
            case StandardStates.Dying:
                DyingBehaviour();
                break;
            case StandardStates.Stunned:
                StunnedBehaviour();
                break;
            case StandardStates.Feared:
                FearedBehaviour();
                break;
            case StandardStates.Hacked:
                HackedBehaviour();
                break;
            case StandardStates.Entering:
                EnteringBehaviour();
                break;
        }
    }

    public virtual void Damage(float damage, bool showDamage, Entity damageDealer = null)
    {
        if (!isInvulnerable)
        {
            health = Mathf.Clamp(health - damage, 0f, maxHealth);
            spriteRenderer.color = damageColor;
            if (showDamage) Util.WorldCanvas.CreateDamageNumber(Mathf.RoundToInt(damage), C.red, (Vector2)transform.position + (Random.insideUnitCircle * 0.2f) + Vector2.up * 0.5f);
            if (damageDealer)
            {
                lastDamageDealingEntity = damageDealer;
                lastDamageDealingTeam = damageDealer.team;
            }

            if (health == 0f) Die();
        }
        else
        {
            Util.WorldCanvas.CreateDamageNumber("Invulnerable", C.white, (Vector2)transform.position + (Random.insideUnitCircle * 0.2f) + Vector2.up * 0.5f);
        }
    }

    public virtual void Damage(float damage, bool showDamage, Team team)
    {
        if (!isInvulnerable)
        {
            health = Mathf.Clamp(health - damage, 0f, maxHealth);
            spriteRenderer.color = damageColor;
            if (showDamage) Util.WorldCanvas.CreateDamageNumber(Mathf.RoundToInt(damage), C.red, (Vector2)transform.position + (Random.insideUnitCircle * 0.2f) + Vector2.up * 0.5f);
            lastDamageDealingTeam = team;
            if (health == 0f) Die();
        }
        else
        {
            Util.WorldCanvas.CreateDamageNumber("Invulnerable", C.white, (Vector2)transform.position + (Random.insideUnitCircle * 0.2f) + Vector2.up * 0.5f);
        }
    }

    public void Knockback(Vector2 position, float power)
    {
        var direction = (Vector2)transform.position - position;
        direction.Normalize();
        direction *= power;
        body.AddForce(direction, ForceMode2D.Impulse);
    }

    public void Heal(float heal)
    {
        health = Mathf.Clamp(health + heal, 0f, maxHealth);
        Util.WorldCanvas.CreateDamageNumber(Mathf.RoundToInt(heal), C.green, (Vector2)transform.position + (Random.insideUnitCircle * 0.2f) + Vector2.up * 0.5f);
    }

    protected virtual void SearchForTarget()
    {
        Entity closestEntity = null;
        var closestDistance = Mathf.Infinity;
        var highestPriority = 0;

        var entitiesFromOpposingTeam = GetEntitiesFromOpposingTeam();
        foreach (var entity in entitiesFromOpposingTeam)
        {
            var distance = Vector2.Distance(transform.position, entity.transform.position);
            if (distance <= aggroRadius && distance < closestDistance && entity.priority >= highestPriority)
            {
                closestEntity = entity;
                closestDistance = distance;
                highestPriority = entity.priority;
            }
        }

        if (closestEntity != null)
        {
            target = closestEntity;

            if (canChase)
                state = StandardStates.Chasing;
            else
                state = StandardStates.Attacking;
        }
    }

    protected virtual void ChasingBehaviour()
    {
        // Go back to idle when there is no target
        if (!target) state = StandardStates.Idle;

        // Aim the weapon
        if (currentWeapon && !currentWeapon.isAttacking)
            UpdateAim(MathEx.AngleFromVector(target.transform.position - transform.position));

        // When there is a target, chase it until attack range
        if ((Vector2)pathfinder.destination != (Vector2)target.transform.position)
        {
            pathfinder.destination = target.transform.position;
        }

        // Check for attack range distance and line of sight
        if (Vector2.Distance(transform.position, target.transform.position) <= attackRange)
        {
            var layerMask = LayerMask.NameToLayer("Collision");
            if (!Physics2D.Raycast(transform.position, (target.transform.position - transform.position), attackRange, layerMask, -1000f, 1000f))
            {
                pathfinder.destination = transform.position;
                state = StandardStates.Attacking;
            }
            else
            {
                Debug.Log("Nothing");
            }
        }
    }

    protected virtual void IdleBehaviour()
    {
        // Patrol when possible
        if (canPatrol)
        {
            state = StandardStates.Patrolling;
        }
    }

    protected virtual void AttackingBehaviour()
    {
        attackCooldown -= Time.deltaTime;

        // Aim the weapon
        if (!currentWeapon.isAttacking)
            UpdateAim(MathEx.AngleFromVector(target.transform.position - transform.position));

        // Prioritize ability casting
        foreach (var ability in abilities)
        {
            // Cast the first ability that is ready
            if (!ability.IsOnCooldown)
            {
                SetCastingState(ability);
            }
        }

        // When no mana for abilities, normal attack
        if (attackCooldown <= 0f)
        {
            currentWeapon.Attack();
            attackCooldown = ATTACK_COOLDOWN;
        }

        // Stand still for a moment and the move again
        if (attackCooldown > 0f)
        {
            attackMovementDelay -= Time.deltaTime;
            if (attackMovementDelay <= 0f)
            {
                attackMovementDelay = ATTACK_MOVEMENT_DELAY;
                state = StandardStates.Chasing;
            }
        }
    }

    protected virtual void PatrollingBehaviour()
    {
        if ((Vector2)pathfinder.destination == nextPatrolPoint)
        {
            patrolCurrentWaitTime -= Time.deltaTime;
            if (patrolCurrentWaitTime <= 0f)
            {
                patrolCurrentWaitTime = Random.Range(patrolMinRandomWaitTime, patrolMaxRandomWaitTime);
                nextPatrolPoint = startPosition + Random.insideUnitCircle * patrolRadius;
            }
        }
        else if ((Vector2)pathfinder.destination != nextPatrolPoint || pathfinder.reachedEndOfPath)
        {
            pathfinder.destination = nextPatrolPoint;
        }
    }

    protected virtual void StunnedBehaviour()
    {
        stunDuration -= Time.deltaTime;
        if (stunDuration <= 0f) state = StandardStates.Idle;
        input = Vector2.zero;
    }

    protected virtual void DyingBehaviour()
    {
        Destroy(gameObject); // Override to give it different behaviour
    }

    protected virtual void FearedBehaviour()
    {
        fearDuration -= Time.deltaTime;
        if (fearDuration <= 0f) state = StandardStates.Idle;
        // TODO move around erratically
        // TODO add effect
    }

    protected virtual void CastingBehaviour()
    {
        // Needs to have an ability to cast
        if (!currentlyCastedAbility) state = StandardStates.Idle;

        // Start casting
        currentlyCastedAbility.Cast();

    }

    protected virtual void HackedBehaviour()
    {
        hackedDuration -= Time.deltaTime;
        if (hackedDuration <= 0f) state = StandardStates.Idle;
        team = (TEAM == Team.Allied) ? Team.Hostile : Team.Allied;
    }

    protected virtual void EnteringBehaviour()
    {
        // Empty
    }

    // Start casting an ability
    public virtual void SetCastingState(Ability abilityToCast)
    {
        state = StandardStates.Casting;
        attackCooldown = ATTACK_COOLDOWN;
        currentlyCastedAbility = abilityToCast;
        abilityToCast.Cast();
        abilityToCast.castFinishedAction = () => { state = StandardStates.Idle; };
    }

    protected virtual void UpdateMovement()
    {
        if (body)
        {
            var force = new Vector2(input.x, input.y) * movementSpeed * Time.deltaTime;
            body.AddForce(force, ForceMode2D.Force);
        }
    }

    public virtual void UpdateAim(float hInput, float vInput)
    {
        // Get the direction vector normalized for multiplication
        aimVector.x = hInput;
        aimVector.y = vInput;
        aimVector.Normalize();

        // Adjust the aim angle
        aimAngle = MathEx.AngleFromVector(aimVector);
    }

    public virtual void UpdateAim(float angle)
    {
        // Make the necessary calculation to store the angle in a vector and clamp it
        var clampedAngle = MathEx.Clamp0360(angle);
        var angleVector = MathEx.VectorFromAngle(angle);

        // Get the direction vector normalized for multiplication
        aimVector.x = angleVector.x;
        aimVector.y = angleVector.y;
        aimVector.Normalize();

        // Adjust the aim angle
        aimAngle = clampedAngle;
    }

    public virtual void Die()
    {
        state = StandardStates.Dying;
        RemoveFromTeam();
    }

    protected void AddToTeam()
    {
        switch (team)
        {
            default:
            case Team.Allied:
                Util.GameManager.alliedEntities.Add(this);
                break;
            case Team.Hostile:
                Util.GameManager.hostileEntities.Add(this);
                break;
            case Team.Neutral:
                Util.GameManager.neutralEntities.Add(this);
                break;
        }
    }

    protected void RemoveFromTeam()
    {
        switch (team)
        {
            default:
            case Team.Allied:
                Util.GameManager.alliedEntities.Remove(this);
                break;
            case Team.Hostile:
                Util.GameManager.hostileEntities.Remove(this);
                break;
            case Team.Neutral:
                Util.GameManager.neutralEntities.Remove(this);
                break;
        }
    }

    protected virtual void UpdateEffects()
    {
        for (int i = buffs.Count - 1; i >= 0; i--)
        {
            var buff = buffs[i];
            buff.Update();
        }

        for (int i = debuffs.Count - 1; i >= 0; i--)
        {
            var debuff = debuffs[i];
            debuff.Update();
        }
    }

    public void AddEffect(Effect effect)
    {
        if (effect is EffectPositive) buffs.Add(effect as EffectPositive);
        else if (effect is EffectNegative) debuffs.Add(effect as EffectNegative);
    }

    public void RemoveEffect(Effect effect)
    {
        if (effect is EffectPositive) buffs.Remove(effect as EffectPositive);
        else if (effect is EffectNegative) debuffs.Remove(effect as EffectNegative);
    }

    protected List<Entity> GetEntitiesFromOpposingTeam()
    {
        if (team == Team.Hostile) return Util.GameManager.alliedEntities;
        else if (team == Team.Allied) return Util.GameManager.hostileEntities;

        return Util.GameManager.neutralEntities;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, aggroRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        var doc = collision.GetComponent<DamageOnContact>();
        if (doc && doc.team != team)
        {
            Damage(doc.damage, true);
            if (!isInvulnerable) Knockback(doc.transform.position, doc.knockbackPower);
        }
    }
}
