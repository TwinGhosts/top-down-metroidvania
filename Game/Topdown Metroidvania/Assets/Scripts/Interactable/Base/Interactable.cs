﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Interactable : MonoBehaviour
{
    protected Rewired.Player inputController;

    public bool isActive = true;
    public bool disableOnInteraction = false;

    protected virtual void Awake()
    {
        inputController = Rewired.ReInput.players.GetPlayer(0);
    }

    public abstract void Interact();

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EntityPlayer>() && inputController.GetButtonDown("Interact") && isActive)
        {
            Interact();
            if (disableOnInteraction) isActive = false;
        }
    }

    protected void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<EntityPlayer>() && inputController.GetButtonDown("Interact") && isActive)
        {
            Interact();
            if (disableOnInteraction) isActive = false;
        }
    }
}
