﻿using UnityEngine;
using System.Collections;

public class AbilityRoll : Ability
{
    [Header("Roll Specifics")]
    public float rollDuration = 0.4f;
    public float rollSpeed = 10f;
    [HideInInspector] public Vector2 rollDirection;
    private bool effectActive = false;
    private float currentRollDuration = 0f;

    public AbilityRollPhasedEntity effect;
    public float phaseEntityInterval = 0.05f;

    private float currentPhaseDuration = 0f;
    private Vector2 preRollPosition;

    protected override void Update()
    {
        base.Update();

        // Add force to the player when activated
        if (effectActive)
        {
            // Count up to the roll duration and add force
            currentRollDuration += Time.deltaTime;
            currentPhaseDuration += Time.deltaTime;

            owner.body.AddForce(rollDirection.normalized * rollSpeed * Time.deltaTime, ForceMode2D.Force);
            owner.isInvulnerable = true;

            // Spawn effect
            if (currentPhaseDuration >= phaseEntityInterval)
            {
                currentPhaseDuration = 0f;
                var image = Instantiate(effect, transform.position, Quaternion.identity);
                var playerRenderer = EntityPlayer.Instance.GetComponentInChildren<SpriteRenderer>();
                image.GetComponent<SpriteRenderer>().flipX = playerRenderer.flipX;
                image.GetComponent<SpriteRenderer>().sprite = playerRenderer.sprite;
            }

            // When the duration has been reached, stop the effect
            if (currentRollDuration >= rollDuration)
            {
                effectActive = false;
                owner.isInvulnerable = false;
                owner.enableInput = true;
                Util.PitCollider.enabled = true;

                // Check for pit collider underneath
                var collisionCheck = Physics2D.OverlapCircleAll(Util.Player.transform.position, 0.01f, 1 << LayerMask.NameToLayer("Collision"));
                foreach (var collision in collisionCheck)
                {
                    Debug.Log("Check + " + collision.name);
                    // Check for pit
                    var componentCheck = collision.GetComponent<TilemapColliderUtil>();
                    if (componentCheck && componentCheck.type.Equals(TileMapType.Pit))
                    {
                        // TODO: make it nicer
                        Util.Player.Damage(Util.Player.maxHealth / 10f, true, null);
                        if (Util.Player.health > 0f) Util.Player.transform.position = preRollPosition;
                        return;
                    }
                }
            }
        }
    }

    protected override void CastAction()
    {
        // Has to have an owner to roll
        if (owner)
        {
            preRollPosition = Util.Player.transform.position;
            owner.body.velocity = Vector2.zero;
            rollDirection = owner.currentInput;
            currentRollDuration = 0f;
            effectActive = true;
            Util.PitCollider.enabled = false;
            owner.enableInput = false;
        }
    }
}
