﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Effect
{
    public string name = "Effect Name";
    public string description = "Effect Description";

    public float duration = 0f;
    public float durationMax;
    public Entity target;

    public UnityEvent OnExpiration = new UnityEvent();
    public UnityEvent OnApplication = new UnityEvent();

    public Effect (float duration)
    {
        durationMax = duration;
        OnApplication.Invoke();
    }

    public virtual void Update ()
    {
        duration += Time.deltaTime;

        // When the effect expires, do expiration actions
        if (duration >= durationMax)
        {
            OnExpiration.Invoke();
            target.RemoveEffect(this);
        }
    }
}
