﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer : Entity
{
    private Rewired.Player inputController;
    private readonly float deadZoneCorrectionValue = 0.2f;
    private bool isUpdatingAngleAim = false;

    [HideInInspector] public Inventory inventory;
    [HideInInspector] public Vector2 enterTarget;
    [Header("Abilities")]
    public Ability abilityRoll;

    public static EntityPlayer Instance;
    private bool runOnce = true;

    protected override void Awake()
    {
        // Persistence
        if (Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            base.Awake();
            Util.Player = this;
            inputController = Rewired.ReInput.players.GetPlayer(0);
        }
    }

    protected override void Start()
    {
        AddToTeam();

        inventory = Util.GameManager.GetComponent<Inventory>();

        // Set the initial aim
        UpdateAim(0f, 1f);
    }

    protected override void Update()
    {
        base.Update();

        // Check for player input
        CheckInput();

        // TEST DAMAGE // TODO: Remove
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Damage(5f, false);

            Debug.Log(GameOptions.Instance.lastRestedScene + " / " + GameOptions.Instance.currentRoom + " / " + GameOptions.Instance.restLocationID);
        }
    }

    protected void LateUpdate()
    {
        if (runOnce)
        {
            // Make the weapon of the player persistent
            if (currentWeapon)
            {
                currentWeapon.transform.SetParent(null);
                DontDestroyOnLoad(currentWeapon.gameObject);
            }

            runOnce = false;
        }
    }

    protected override void EnteringBehaviour()
    {
        enableInput = false;
        isInvulnerable = true;

        // Move to enter target
        if (Vector2.Distance(transform.position, enterTarget) > 0.1f)
        {
            input = (enterTarget - (Vector2)transform.position).normalized * 0.75f;
        }
        else
        {
            input = Vector2.zero;
            enableInput = true;
            state = StandardStates.None;
            isInvulnerable = false;
        }
    }

    public override void UpdateAim(float hInput, float vInput)
    {
        // Has to be above deadzone value to be affected
        var directionCheck = new Vector2(hInput, vInput).magnitude;
        if (directionCheck <= deadZoneCorrectionValue) return;

        // Get the direction vector normalized for multiplication
        aimVector.x = hInput;
        aimVector.y = vInput;
        aimVector.Normalize();

        // Adjust the aim angle
        aimAngle = MathEx.AngleFromVector(aimVector);
    }

    public override void UpdateAim(float angle)
    {
        base.UpdateAim(angle);
        isUpdatingAngleAim = true;
    }

    protected void CheckInput()
    {
        // Moving the player around with a force in the movement direction
        if (enableInput)
        {
            input.x = inputController.GetAxisRaw(0);
            input.y = inputController.GetAxisRaw(1);

            // Handle the player's aiming
            var inputH = inputController.GetAxisRaw("Move Aim Horizontal");
            var inputV = inputController.GetAxisRaw("Move Aim Vertical");
            if (isUpdatingAngleAim) { isUpdatingAngleAim = false; return; }
            if (inputH != 0f && inputV != 0f)
            {
                UpdateAim(inputH, inputV);
            }
        }

        if (input != Vector2.zero) currentInput = input;
        var force = new Vector2(input.x, input.y) * movementSpeed * Time.deltaTime;

        // Sprite flipping
        if (aimAngle < 90f && aimAngle >= -90f) spriteRenderer.flipX = false;
        else spriteRenderer.flipX = true;

        body.AddForce(force, ForceMode2D.Force);

        // Attacking with an equiped weapon
        if (enableInput && currentWeapon && currentWeapon.hasControl && inputController.GetButtonDown("Attack"))
        {
            if (!(currentWeapon is WeaponBow))
            {
                currentWeapon.Attack();
            }
        }

        // Bow charge
        if (enableInput && currentWeapon && currentWeapon.hasControl && currentWeapon is WeaponBow)
        {
            var bow = currentWeapon as WeaponBow;
            if (inputController.GetButton("Attack"))
            {
                bow.Charge();
            }
            else if (inputController.GetButtonUp("Attack"))
            {
                bow.Attack();
            }
        }

        // Check for ability inputs
        if (enableInput && inputController.GetButtonDown("Dodge"))
        {
            abilityRoll.Cast();
        }

        // Idle Anim
        if (InputIsInDeadZone())
        {
            animator.Play("Player Idle");
            animator.speed = 1f;
        }
        // Walking anim
        else
        {
            animator.speed = 0.5f + (0.5f * input.magnitude);
            if (input.x >= 0f && !spriteRenderer.flipX)
            {
                animator.Play("Player Walk");
            }
            else if (input.x < 0f && !spriteRenderer.flipX)
            {
                animator.Play("Player Walk Reverse");
            }
            else if (input.x < 0f && spriteRenderer.flipX)
            {
                animator.Play("Player Walk");
            }
            else
            {
                animator.Play("Player Walk Reverse");
            }
        }
    }

    public bool InputIsInDeadZone()
    {
        var deadZone = 0.05f;
        return (input.x > -deadZone && input.x < deadZone && input.y > -deadZone && input.y < deadZone);
    }

    public override void Damage(float damage, bool showDamage, Entity damageDealer = null)
    {
        base.Damage(damage, showDamage, damageDealer);
        if (!isInvulnerable)
        {
            ProCamera2DShake.Instance.Shake(0.1f, Vector2.one * 2f);
            inputController.SetVibration(0, 0.5f, 0.3f, false);
        }
    }

    public override void Damage(float damage, bool showDamage, Team team)
    {
        base.Damage(damage, showDamage, team);
        if (!isInvulnerable)
        {
            ProCamera2DShake.Instance.Shake(0.1f, Vector2.one * 2f);
            inputController.SetVibration(0, 0.5f, 0.3f, false);
        }
    }

    public override void Die()
    {
        // Disable the player
        enableInput = false;
        spriteRenderer.enabled = false;
        input = Vector2.zero;
        ProCamera2DShake.Instance.Shake(0.3f, Vector2.one * 4f);

        Util.Player.isInvulnerable = false;

        Util.Player.buffs.Clear();
        Util.Player.debuffs.Clear();

        // Go to the last rested scene
        Util.FadeManager.GoToScene(GameOptions.Instance.lastRestedScene);
        Util.respawnOnLoad = true;
    }

    public void UpgradeHealth(float newMaxValue)
    {
        maxHealth = newMaxValue;
        Util.HUD.RenewHealthBar();
    }
}
