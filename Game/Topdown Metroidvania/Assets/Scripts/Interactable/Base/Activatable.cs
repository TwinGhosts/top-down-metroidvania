﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Activatable : MonoBehaviour
{
    public bool isActive = true;

    public UnityEvent OnActivate = new UnityEvent();
    public UnityEvent OnDeactivate = new UnityEvent();

    public virtual void Activate (bool activate)
    {
        isActive = activate;
        if (activate) OnActivate.Invoke();
        else OnDeactivate.Invoke();
    }
}
