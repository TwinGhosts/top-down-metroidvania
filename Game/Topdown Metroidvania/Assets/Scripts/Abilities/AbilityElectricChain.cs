﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilityElectricChain : Ability
{
    [Header("Chain Specifics")]
    public int maxChainLinks = 20;
    public float chainLinkSpawnInterval = 0.3f;
    public float chainDistance = 0.5f;
    public ElectricChainObject linkObject;

    private List<ElectricChainObject> chainLinks = new List<ElectricChainObject>();
    private float direction = 0f;

    protected override void Update()
    {
        base.Update();
    }

    public void SetDirection(float dir)
    {
        direction = dir;
    }

    public override void Cast()
    {
        SetDirection(GetComponentInParent<Entity>().aimAngle);

        base.Cast();
    }

    protected override void CastAction()
    {
        // Has to have an owner to roll
        StartCoroutine(_CreateLinks());
    }

    private IEnumerator _CreateLinks()
    {
        // Reset
        chainLinks.Clear();

        // Get the direction to aim in as a vector
        var directionVector = MathEx.VectorFromAngle(direction).normalized;
        var count = 1;

        // Try to create links until the max amount
        while (chainLinks.Count < maxChainLinks)
        {
            var firstSpawnPosition = owner.transform.position + (directionVector * chainDistance * count);
            var spawnPosition = Vector3.zero;
            if (count > 1) spawnPosition = chainLinks[count - 2].transform.position + (directionVector * chainDistance);
            var layerMask = 1 << LayerMask.NameToLayer("Collision");
            var check = Physics2D.OverlapCircle((count == 1) ? firstSpawnPosition : spawnPosition, 0.125f, layerMask);

            // Position must not be in a wall
            if (!check)
            {
                var link = Instantiate(linkObject, (count == 1) ? firstSpawnPosition : spawnPosition, Quaternion.identity);
                link.owner = owner;
                link.team = owner.team;
                chainLinks.Add(link);
                link.Activate();
            }
            else
            {
                break;
            }

            count++;
            yield return new WaitForSeconds(chainLinkSpawnInterval);

            // Safety net
            if (count > maxChainLinks)
            {
                Debug.LogError("Infinite for loop caught");
                break;
            }
        }
    }
}
