﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Activator : Interactable
{
    public int maxActivationCount = 1;
    public List<Activatable> activatables = new List<Activatable>();
    [HideInInspector] public int activationCount = 0;

    public override void Interact ()
    {
        // Activate only a number of times
        if (activationCount < maxActivationCount)
        {
            // Go through all possible activatables and activate them
            foreach (var activatable in activatables)
            {
                activatable.Activate(true); // TODO think about toggling
            }
        }
    }
}
