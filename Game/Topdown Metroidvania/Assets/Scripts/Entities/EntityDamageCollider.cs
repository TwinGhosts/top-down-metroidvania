﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityDamageCollider : MonoBehaviour
{
    private Entity entity;

    private void Awake()
    {
        entity = GetComponentInParent<Entity>();
    }

    // Projectile collision
    private void ProjectileCheck(Projectile projectile)
    {
        // Check how many time the projectile can hit and whether this entity is on the right team to be hit
        if (projectile.targetsHit.Count < projectile.maxTargetsHit && !projectile.targetsHit.Contains(entity) && projectile.owner.team != entity.team)
        {
            // Add it to the list of targets hit to make it not hit double
            projectile.targetsHit.Add(entity);

            // Entity mustn't be invunerable to take damage
            if (!entity.isInvulnerable)
            {
                // Damage the entity and activate the succesfulhit event
                if (projectile.owner)
                    entity.Damage(projectile.damage, true, projectile.owner);
                else
                    entity.Damage(projectile.damage, true, projectile.team);

                projectile.weapon?.owner?.OnSuccesfulWeaponHit.Invoke();
            }
            else
            {
                // When the hit didn't deal damage activate the failed event
                projectile.weapon?.owner?.OnFailedWeaponHit.Invoke();
            }

            // Destroy the projectile when it has hit the max amount of targets it can hit
            if (projectile.targetsHit.Count >= projectile.maxTargetsHit) Destroy(projectile.gameObject);
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        var projectile = other.GetComponent<Projectile>();

        // Damage
        if (projectile)
            ProjectileCheck(projectile);
    }

    protected void OnTriggerStay2D(Collider2D other)
    {
        var projectile = other.GetComponent<Projectile>();

        // Damage
        if (projectile)
            ProjectileCheck(projectile);
    }
}
