﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [HideInInspector] public GameObject dialogueParent;
    [HideInInspector] public Text titleText;
    [HideInInspector] public Text descriptionText;

    private void Awake()
    {
        Util.DialogueManager = this;
    }

    private void Start()
    {
        dialogueParent = Util.HUD.dialogueParent;
        titleText = Util.HUD.dialogueTitleText;
        descriptionText = Util.HUD.dialogueDescriptionText;
    }

    public void SendMessage(string title, string description)
    {
        if (!dialogueParent.activeSelf)
        {
            dialogueParent.SetActive(true);
        }

        titleText.text = title;
        descriptionText.text = description;
    }

    public void Hide()
    {
        if (dialogueParent.activeSelf)
            dialogueParent.SetActive(false);
    }
}
