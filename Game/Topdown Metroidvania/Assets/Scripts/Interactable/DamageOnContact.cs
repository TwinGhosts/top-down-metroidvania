﻿using UnityEngine;
using System.Collections;

public class DamageOnContact : MonoBehaviour
{
    public Team team = Team.Hostile;
    public float damage = 5f;
    public float knockbackPower = 2f;
}
