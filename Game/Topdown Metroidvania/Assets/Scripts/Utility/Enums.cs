﻿#region Teams
public enum Team
{
    Hostile,
    Allied,
    Neutral,
}
#endregion Teams

#region Menu Scenes
public enum MenuScene
{
    Main,
    SaveLoad,
    Options,
    Credits,
}
#endregion Menu Scenes

#region Room Scenes
public enum RoomScene
{
    TestRoomCenter,
    TestRoomNorth,
    TestRoomEast,
    TestRoomSouth,
    TestRoomWest,
}
#endregion Room Scenes

#region Entities
public enum EntityType
{
    Player,
    Scavenger,
    Stalker,
    Trapper,
    Spinner,
}
#endregion Entities

#region Areas
public enum Area
{
    Bunker,
    Wasteland,
    // TODO add more
}
#endregion Areas

public enum EnterDirection
{
    Up,
    Right,
    Down,
    Left,
}

public enum Direction
{
    Up,
    Right,
    Down,
    Left,
}