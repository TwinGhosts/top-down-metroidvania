﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpear : Weapon
{
    [Header("Spear Properties")]
    public Collider2D stabCollider;
    protected float colliderResetTimer = 0f;

    protected override void Update()
    {
        if (colliderResetTimer > 0f) colliderResetTimer -= Time.deltaTime;
        if (colliderResetTimer <= 0f)
        {
            stabCollider.enabled = false;
            hitTargets.Clear();
        }

        base.Update();
    }

    public override void Attack()
    {
        base.Attack();
        if (hasControl)
        {
            hitTargets.Clear();
            stabCollider.enabled = true;
            colliderResetTimer = attackResetTimer;
            owner.OnWeaponHit.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Damage targets
        var entity = collision.GetComponent<Entity>();
        if (entity && !entity.isInvulnerable && !hitTargets.Contains(entity) && entity.team != owner.team)
        {
            hitTargets.Add(entity);
            entity.Damage(damage, owner);
            ProCamera2DShake.Instance.Shake(0.1f, Vector2.one);
            entity.Knockback(owner.transform.position, 15f);
            owner.OnSuccesfulWeaponHit.Invoke();
        }
        else if (entity && entity.isInvulnerable && entity.team != owner.team)
        {
            owner.OnFailedWeaponHit.Invoke();
        }
    }
}
