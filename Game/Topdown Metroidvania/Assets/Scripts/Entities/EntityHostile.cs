﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityHostile : Entity
{
    [Header("Loot")]
    public LootTable lootTable;
    public float dropRadius = 1f;

    [Header("Spawn Properties")]
    public Weapon weaponToSpawn;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();

        if (weaponToSpawn && !currentWeapon)
        {
            currentWeapon = Instantiate(weaponToSpawn);
            currentWeapon.owner = this;
        }

        // Add this as a camera target at the start
        UpdateAim(Random.Range(0, 360));
    }

    public override void Die()
    {
        // Get data from lootTable
        var loot = lootTable.GetLoot();
        var del = lootTable.GetDel();

        // Spawn loot
        foreach (var lootItem in loot)
        {
            var positionRadius = dropRadius * Random.insideUnitSphere;
            positionRadius.z = 0f;
            Instantiate(lootItem.item, transform.position + positionRadius, Quaternion.identity);
        }

        // Spawn gold
        var delPieces = DelUtility.GetDelPieces(del);
        foreach (var delPrefab in delPieces)
        {
            var positionRadius = dropRadius * Random.insideUnitSphere;
            positionRadius.z = 0f;
            Instantiate(delPrefab, transform.position + positionRadius, Quaternion.identity);
        }

        // TODO add spawn animation

        base.Die();
    }

    protected override void Update()
    {
        base.Update();
    }
}
