﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    [Header("Owner")]
    public Entity owner = null;
    [Min(0f)] public float distanceFromOwner = 0.5f;
    protected float startingDistanceFromOwner = 0f;

    [Header("Values")]
    public float damage = 20f;
    [HideInInspector] public float maxDamage;
    public float attackDistanceIncrease = 0.2f;
    public float attackResetTimer = 0.5f;
    [HideInInspector] public float maxAttackResetTimer;
    [HideInInspector] public Collider2D[] damageColliders;
    [HideInInspector] public bool hasControl = true;
    [HideInInspector] public List<Entity> hitTargets = new List<Entity>();
    [HideInInspector] public bool isAttacking = false;

    protected Animator animator;

    protected virtual void Awake()
    {
        startingDistanceFromOwner = distanceFromOwner;
        damageColliders = GetComponents<Collider2D>();
        animator = GetComponentInChildren<Animator>();
        maxDamage = damage;

        maxAttackResetTimer = attackResetTimer;
        attackResetTimer = 0f;
    }

    protected virtual void Start()
    {
        // Store the entities as a parent of the entities object
        var parent = GameObject.Find("Weapons");
        if (parent)
            transform.SetParent(parent.transform);

        if (owner) PositionBasedOnOwner();
    }

    protected virtual void Update()
    {
        if (!owner) { Destroy(gameObject); return; }

        // Count the reset timer down
        attackResetTimer = Mathf.Clamp(attackResetTimer - Time.deltaTime / maxAttackResetTimer, 0f, 1f);

        // Keep the weapon at the right attack position
        distanceFromOwner = Mathf.Lerp(distanceFromOwner, startingDistanceFromOwner, 1f - attackResetTimer);
        distanceFromOwner = Mathf.Clamp(distanceFromOwner, 0f, startingDistanceFromOwner + attackDistanceIncrease);

        PositionBasedOnOwner();
        var finalSortingOrder = owner.spriteRenderer.sortingOrder + ((owner.aimAngle >= 0f) ? -1 : 1);
        GetComponentInChildren<SpriteRenderer>().sortingOrder = finalSortingOrder;
    }

    public virtual void Attack()
    {
        // Can only attack when control has been given
        if (hasControl)
        {
            distanceFromOwner += attackDistanceIncrease;
            attackResetTimer = 1f;
        }
    }

    protected void PositionBasedOnOwner()
    {
        if (owner)
        {
            // Position at the aim direction of the owner
            transform.position = (Vector2)owner.weaponAnchor.position + owner.aimVector * distanceFromOwner;
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, owner.aimAngle - 90f));
        }
    }
}
