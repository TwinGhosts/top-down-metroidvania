﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LootTable : MonoBehaviour
{
    [Header("Del")]
    public int minDel = 0;
    public int maxDel = 100;

    [Header("Items")]
    public List<Loot> possibleLoot = new List<Loot>();

    public int GetDel ()
    {
        return Random.Range(minDel, maxDel);
    }

    public List<Loot> GetLoot ()
    {
        var lootList = new List<Loot>();

        // Go through all possible loot and do chance rolls
        foreach (var loot in possibleLoot)
        {
            // Rool per loot item
            if (Random.Range(0, 100) <= loot.chance)
            {
                lootList.Add(loot);
            }
        }

        return lootList;
    }
}

[System.Serializable]
public class Loot
{
    [Range(0, 100)]
    public int chance = 0;
    public Item item;

    public Loot (int chance, Item item)
    {
        this.chance = chance;
        this.item = item;
    }
}