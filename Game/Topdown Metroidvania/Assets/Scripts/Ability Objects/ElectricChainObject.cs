﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class ElectricChainObject : MonoBehaviour
{
    [Header("Chain Phase")]
    public float chainTime = 10f;

    [Header("Shake Phase")]
    public float releaseTime = 3f;
    public float shakeStartingPower = 0.001f;
    public float shakeIncrement = 0.005f;

    [Header("Explosion Phase")]
    public Projectile explosionProjectile;
    public int projectileAmount = 2;
    public float projectileSpeed;
    public float projectileDamage;
    public float projectileLifeTime;

    [Header("Particle Systems")]
    public ParticleSystem ps_electric;
    public ParticleSystem ps_explosion;

    [HideInInspector] public Entity owner;
    [HideInInspector] public Team team;

    private bool active = false;
    private bool release = false;

    private SpriteRenderer spriteRenderer;

    private float timer = 0f;

    private Vector2 startPosition;

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        startPosition = transform.position;
    }

    private void Update()
    {
        // Let the chain be active for a specific amount of time before it shoots away
        if (active)
        {
            if (chainTime > 0f)
            {
                chainTime -= Time.deltaTime;
            }
            // After the initial wait is over, start the next phase
            else if (chainTime <= 0f)
            {
                active = false;
                release = true;
            }
        }

        // Fade the chain out and push it away
        if (release)
        {
            // Shake object
            shakeStartingPower += shakeIncrement * Time.deltaTime;
            transform.position = startPosition + Random.insideUnitCircle.normalized * shakeStartingPower;
            timer += Time.deltaTime;
        }

        // Delete when death timer expires
        if (timer >= releaseTime)
        {
            ps_explosion.transform.SetParent(null);
            ps_explosion.Play();

            for (int i = 0; i < projectileAmount; i++)
            {
                var projectile = Instantiate(explosionProjectile, transform.position, Quaternion.identity);
                projectile.Init(projectileSpeed, projectileDamage, Random.Range(0, 360), owner, null, projectileLifeTime);
                projectile.updateAngle = true;
            }

            Destroy(gameObject);
        }
    }

    public void Activate()
    {
        active = true;
    }
}
