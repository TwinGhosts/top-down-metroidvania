﻿using UnityEngine;
using System.Collections;

public class EffectHealOverTime : EffectPositive
{
    private int ticks = 5;
    private int currentTick = 1;
    private float tickInterval;
    private bool tickOnApplication = true;
    private float healPerTick;

    public EffectHealOverTime(Entity target, float duration, float healPerTick, int ticks, bool firstTick) : base(duration)
    {
        this.healPerTick = healPerTick;
        this.ticks = ticks;
        this.target = target;
        tickOnApplication = firstTick;

        // Calculate the amount of ticks
        tickInterval = duration / ticks;

        // Tick once when needed
        if (tickOnApplication)
        {
            target.Heal(healPerTick);
        }
    }

    public override void Update()
    {
        base.Update();

        // Invoke the tick effect
        if (duration > tickInterval * currentTick)
        {
            currentTick++;
            target.Heal(healPerTick);
        }
    }
}