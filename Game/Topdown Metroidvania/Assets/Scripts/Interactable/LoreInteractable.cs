﻿using UnityEngine;

public class LoreInteractable : Interactable
{
    public new string name = "Lore Excerpt";
    [TextArea] public string description = "Random Description";

    public override void Interact()
    {
        if (!isActive)
        {
            Util.DialogueManager.SendMessage(name, description);
            isActive = true;
        }
        else
        {
            Util.DialogueManager.Hide();
            isActive = false;
        }
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<EntityPlayer>())
        {
            Util.DialogueManager.Hide();
        }
    }
}
