﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.Tilemaps;

public class SceneUtilityEditor : EditorWindow
{
    /*
    private string sceneName = "New Scene";

    private bool groupEnabled;
    static SceneUtilityEditor window;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/The Vagrant Knight Utility/Create Scene")]
    static void CreateScene()
    {
        // Get existing open window or if none, make a new one:
        window = (SceneUtilityEditor)GetWindow(typeof(SceneUtilityEditor));
        window.name = "Scene Creation";
        window.Show();

        string[] path = EditorSceneManager.GetActiveScene().path.Split(char.Parse("/"));
        Debug.Log(path);
    }

    private void OnGUI()
    {
        GUILayout.Label("Create Preconfigured Scene", EditorStyles.boldLabel);
        sceneName = EditorGUILayout.TextField("Scene Name", sceneName);

        var createSceneButton = GUI.Button(new Rect(10, 100, 250, 25), "Create New Scene");
        if (createSceneButton)
        {
            var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
            newScene.name = sceneName;

            EditorSceneManager.SaveScene(newScene, "Assets/Scenes/" + sceneName + ".unity");

            // Basics
            var camera = PrefabUtility.InstantiatePrefab(Resources.Load("Prefabs/Camera/Camera_ Main")); camera.name = "Camera: Main";
            var inputManager = PrefabUtility.InstantiatePrefab(Resources.Load<Rewired.InputManager>("Prefabs/Input/Rewired Input Manager")); inputManager.name = "Rewired Input Manager";
            var canvas = PrefabUtility.InstantiatePrefab(Resources.Load<Canvas>("Prefabs/UI/Canvas_ UI")); canvas.name = "Canvas: UI";
            var gameManager = PrefabUtility.InstantiatePrefab(Resources.Load<GameManager>("Prefabs/Managers/Game Manager")); gameManager.name = "Game Manager";
            var pathfinding = PrefabUtility.InstantiatePrefab(Resources.Load<GameManager>("Prefabs/AI/AStar Pathfinding")); gameManager.name = "Astar Pathfinding";

            // Tile maps
            var gridWorld = new GameObject("Grid: World"); gridWorld.AddComponent<Grid>();
            var tilemapCollision = new GameObject("Tilemap: Collision");
            tilemapCollision.transform.SetParent(gridWorld.transform);
            tilemapCollision.AddComponent<Tilemap>();
            tilemapCollision.AddComponent<TilemapRenderer>();
            tilemapCollision.AddComponent<TilemapCollider2D>();

            var tilemapDecoration = new GameObject("Tilemap: Decoration");
            tilemapDecoration.transform.SetParent(gridWorld.transform);
            tilemapDecoration.AddComponent<Tilemap>();
            tilemapDecoration.AddComponent<TilemapRenderer>();

            // Entities and weapons
            var itemParent = new GameObject("Items");
            var entityParent = new GameObject("Entities");
            var weaponParent = new GameObject("Weapons");

            var player = PrefabUtility.InstantiatePrefab(Resources.Load<EntityPlayer>("Prefabs/Entities/Entity_ Player"), entityParent.transform); player.name = "Entity: Player";

            window.Close();
        }
    }
    */
}