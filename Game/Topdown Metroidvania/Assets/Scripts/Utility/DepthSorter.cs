﻿using UnityEngine;
using System.Collections;

public class DepthSorter : MonoBehaviour
{
    public Transform depthSortTransform;
    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        if (!depthSortTransform) depthSortTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (depthSortTransform) spriteRenderer.sortingOrder = (int)(-depthSortTransform.position.y * 32);
    }
}
