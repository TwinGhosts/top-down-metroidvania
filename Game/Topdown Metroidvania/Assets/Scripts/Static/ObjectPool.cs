﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool<T> : MonoBehaviour where T : MonoBehaviour
{
    public T pooledObjectType;
    public int poolSize = 10;

    [HideInInspector] public List<T> activeObjects = new List<T>();
    [HideInInspector] public List<T> inactiveObjects = new List<T>();

    private IEnumerator Start ()
    {
        // Create the objects
        for (int i = 0; i < poolSize; i++)
        {
            var spawnedObject = Instantiate(pooledObjectType, Vector2.zero, Quaternion.identity);
            yield return spawnedObject;
            inactiveObjects.Add(spawnedObject);
        }
    }
}

public static class Pool
{
    public static Dictionary<string, ObjectPool<MonoBehaviour>> pools = new Dictionary<string, ObjectPool<MonoBehaviour>>();

    // TODO
}