﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Graphic delGraphic;
    public Text delText;
    public RectTransform hpBarParent;
    [HideInInspector] public List<UIHealthObject> healthBlocks = new List<UIHealthObject>();

    private float delProgress = 0f;
    public float showDelTimer = 4f;
    private float delTimerMax;

    [Header("Dialogue")]
    public GameObject dialogueParent;
    public Text dialogueTitleText;
    public Text dialogueDescriptionText;

    private void Awake()
    {
        Util.HUD = this;
        Util.DelGatherPoint = delGraphic.gameObject;
        delTimerMax = showDelTimer;
    }

    private void Start()
    {
        if (!Util.Player) return;

        RenewHealthBar();
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Util.Player) return;

        if (Time.frameCount % 4 == 0)
        {
            UpdateHealth();
            delText.text = " " + Util.Player.inventory.del;
        }

        DelUpdate();
    }

    private void DelUpdate()
    {
        if (delProgress > 0f)
        {
            delProgress -= Time.deltaTime;
        }

        if (showDelTimer > 0f)
        {
            var color = delGraphic.color;
            var color2 = delText.color;

            showDelTimer -= Time.deltaTime;

            color.a = Mathf.Lerp(0f, 1f, showDelTimer);
            color2.a = color.a;

            delGraphic.color = color;
            delText.color = color2;
        }

        delGraphic.rectTransform.localScale = Vector3.one + Vector3.one * (delProgress / 1.5f);
    }

    public void AddDel()
    {
        delProgress = 0.2f;
        showDelTimer = delTimerMax;
    }

    public void RenewHealthBar()
    {
        var maxHealth = Util.Player.maxHealth;
        var health = Util.Player.health;
        int amountOfBlocks = (int)(maxHealth / Util.playerHealthIncrement);

        // Add more blocks of health if needed
        while (healthBlocks.Count < amountOfBlocks)
        {
            var block = Instantiate(Util.PrefabHolder.healthObject, hpBarParent);
            healthBlocks.Add(block);
            block.id = healthBlocks.Count;
        }

        UpdateHealth();
    }

    public void UpdateHealth()
    {
        var maxHealth = Util.Player.maxHealth;
        var health = Util.Player.health;
        int amountOfBlocks = (int)(maxHealth / Util.playerHealthIncrement);
        int currentBlock = Mathf.FloorToInt((health / maxHealth) * amountOfBlocks); // Has to be one lower than the actual, for array purposes

        var currentBlockAmount = health - ((currentBlock) * Util.playerHealthIncrement);
        var currentBlockIncrement = Mathf.RoundToInt(currentBlockAmount / (Util.playerHealthIncrement / 8f));

        // Go through the blocks and update the graphics
        foreach (var block in healthBlocks)
        {
            block.image.sprite = Util.PrefabHolder.healthFractions[8];

            if (block.id > currentBlock + 1)
            {
                block.image.sprite = Util.PrefabHolder.healthFractions[0];
            }
            else if (block.id == currentBlock + 1)
            {
                block.image.sprite = Util.PrefabHolder.healthFractions[currentBlockIncrement];
            }
        }
    }
}
