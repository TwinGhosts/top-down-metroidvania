﻿using UnityEngine;
using System.Collections;

public class PickUpHealingPotion : Pickup
{
    public float healthAmountPerTick = 10f;
    public float duration = 5f;
    public int ticks = 3;
    public bool firstTickActive = true;
}
