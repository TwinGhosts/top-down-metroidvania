﻿using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneUtility
{
    public static Scene GetScene(RoomScene scene)
    {
        var sceneName = StringUtil.AddSpacesToSentence(scene.ToString());

        Debug.Log(sceneName);

        return SceneManager.GetSceneByName(scene.ToString());
    }
}
