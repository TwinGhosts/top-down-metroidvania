﻿using UnityEngine;
using System.Collections;

public class EntityTrapper : EntityHostile
{
    protected override void Start ()
    {
        base.Start();

        state = StandardStates.Patrolling;
    }

    protected override void Update ()
    {
        base.Update();
    }

    protected override void StateCheck ()
    {
        base.StateCheck();
    }

    protected override void AttackingBehaviour ()
    {
        base.AttackingBehaviour();
    }
}
